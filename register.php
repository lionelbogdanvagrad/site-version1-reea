<?php
include_once('config.php');
//include_once('User.class.php');

//Methods

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

// user
// user_profile
//$user = new User();
@$user->firstname=$_POST['name'];

//if ($user->validate() && $user->save()) {
  //header("Location: thankyou.php");
//}

$isValid=true;
$errors=array();

if ($_SERVER['REQUEST_METHOD']=='POST') {
  // print_r($_POST);
  if (empty($_POST['name'])) {
    $errors['name']='Name is required';
  }
  if (empty($_POST['forename'])) {
    $errors['forename']='forename is required';
  }
}
$isValid=(count($errors)==0);


// define variables and set to empty values
//$usernameErr = $emailErr = $passwordErr = $confirm_passwordErr = 
//$genderErr = $date_of_birthErr = $zodiaErr = $reteaErr = $notesErr = $myFileErr = $termsErr = "";

$name = $forename = $username = $email = $password = $confirm_password = 
$gender = $date_of_birth = $zodia = $notes = $myFile = $terms = "";
/**
 * Controller
 */
if ($_SERVER["REQUEST_METHOD"] == "POST") {

  $error_count = 0;
  
  if (empty($_POST["name"])) {
    $errors['name'] = "Vă rugăm scrieţi numele dumneavoastră.";
    $error_count++;

  } else {
    $name = test_input($_POST["name"]);
    
    if($name == "") {
      $errors['name'] = "Atenţie! Numele dumneavoastră conţine doar spaţii.";
      $error_count++;

    } elseif (!preg_match("/^[a-zA-Z ]*$/",$name)) {
        // Verificam daca numele contine doar litere si spatii.
        
          $errors['name'] = "Atenţie! Sunt permise doar litere şi spaţii.";
          $error_count++;
        
    } else {
        $errors['name'] = "";
    }

  }

  if (empty($_POST["forename"])) {
    $errors['forename'] = "Vă rugăm scrieţi prenumele dumneavoastră.";
    $error_count++;

  } else {
    $forename = test_input($_POST["forename"]);

    if($forename == "") {
      $errors['forename'] = "Atenţie! Prenumele dumneavoastră conţine doar spaţii.";
      $error_count++;

    } elseif (!preg_match("/^[a-zA-Z ]*$/",$forename)){  
        // Verificam daca prenumele contine doar litere si spatii.
        
          $errors['forename'] = "Atenţie! Sunt permise doar litere şi spaţii.";
          $error_count++;
      } else {
          $errors['forename'] = "";
      }
    }
  
  
  if (empty($_POST["username"])) {
    $errors['username'] = "Vă rugăm scrieţi numele dumneavoastră de utilizator.";
    $error_count++;

  } else {
    $username = test_input($_POST["username"]);

    if($username == "") {
      $errors['username'] = "Atenţie! Numele dumneavoastră de utilizator conţine doar spaţii.";
      $error_count++;

    } elseif ((!preg_match("/^[a-zA-Z0-9 ]*$/",$username))) {
        // Verificam daca numele de utilizator contine doar litere, cifre si spatii.
  
          $errors['username'] = "Atenţie! Sunt permise doar litere, cifre şi spaţii.";
          $error_count++;
           
      } else {
          $errors['username'] = "";
      }
  }


  if (empty($_POST["email"])) {
    $errors['email'] = "Vă rugăm scrieţi e-mail-ul dumneavoastră.";
    $error_count++;
  
  } else {
    $email = test_input($_POST["email"]);
    // Verificam daca adresa de e-mail este valida(contine @ etc.)
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $errors['email'] = "Atenţie! Adresa de e-mail nu este validă."; 
      $error_count++;
    
    } elseif(filter_var($email, FILTER_VALIDATE_EMAIL)){
        $errors['email'] = "";
    }
  }

  if (empty($_POST["password"])) {
    $errors['password'] = "Vă rugăm scrieţi parola dumneavoastră de utilizator.";
    $error_count++;

  } else {
    $password = $_POST['password'];
    if (preg_match("#.*^(?=.{8,20})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$#", $password)){
      $errors['password'] = "Bravo! Parola dumneavoastră este sigură.";
    } else {
        $errors['password'] = "Atenţie! Parola dumneavoastră NU este sigură.";
        $error_count++;
      }
       
    }
  

  if (empty($_POST["confirm_password"])) {
    $errors['confirm_password'] = "Vă rugăm CONFIRMATI parola dumneavoastră de utilizator.";
    $error_count++;

  } else {
    $confirm_password = $_POST['confirm_password'];
    if ($password == $confirm_password){
      $errors['confirm_password'] = "Bravo! Aţi CONFIRMAT cu succes parola dumneavoastră.";
    } else {
        $errors['confirm_password'] = "Atenţie! Parola dumneavoastră NU coincide cu parola iniţială.";
        $error_count++;
      }
       
    }
  
  if (empty($_POST["gender"])) {
    $errors['gender'] = "Vă rugăm selectaţi sexul dumneavoastră.";
    $error_count++;

  } else {
    $gender = test_input($_POST["gender"]);
  } 



  if (empty($_POST["date_of_birth"])) {
    $errors['date_of_birth'] = "Vă rugăm scrieţi data dumneavoastră de naştere.";
    $error_count++;

  } else {

    $date_of_birth = $_POST['date_of_birth'];
    $dob = explode("-", $date_of_birth);

    $year = $dob[0];
    //$month = $dob[1];
    //$day = $dob[2];
  } 


  function validateDate($date_of_birth, $format = 'Y-m-d')
  {
    $d = DateTime::createFromFormat($format, $date_of_birth);
    return $d && $d->format($format) == $date_of_birth;
  }


  if ((validateDate($date_of_birth, $format = 'Y-m-d') == true) && ($year<=2017)) {
    $errors['date_of_birth'] = "Bravo! Aţi introdus o dată de naştere validă.";
  }
  else {
    $errors['date_of_birth'] = "Atenţie! Vă rugăm introduceţi o dată de naştere validă.";
    $error_count++;
  };


  if (empty($_POST["zodia"])) {
    $errors['zodia'] = "Vă rugăm selectaţi zodia dumneavoastră.";
    $error_count++;
    //print_r($_POST);
    //exit;

  } else {
         if(($_POST["zodia"]>=1) && ($_POST["zodia"]<=12)){
        $errors['zodia'] = "";
      }
      else {
        $errors['zodia'] = "Vă rugăm selectaţi zodia dumneavoastră.";
      }
    }


    if (empty($_POST["notes"])) {
      $errors['notes'] = "Vă rugăm scrieţi un comentariu.";
      $error_count++;

    } else {
      $notes = test_input($_POST["notes"]);

      if($notes == "") {
        $errors['notes'] = "Atenţie! Comentariul dumneavoastră conţine doar spaţii.";
        $error_count++;
      } elseif (!preg_match("/^[a-zA-Z0-9 ]*$/",$notes)) {
        // Verificam daca numele contine doar litere, cifre si spatii.
        
          $errors['notes'] = "Atenţie! Sunt permise doar litere, cifre şi spaţii.";
          $error_count++;
        
    } else {
        $errors['notes'] = "";
      }
    }

    if (empty($_POST["terms"])) {
    $errors['terms'] = "Vă rugăm acceptaţi Termenii şi Condiţiile.";
    $error_count++;
   } else {
      $errors['terms'] = "";
   } 

    





$target_dir = "version1/images/";
$target_file = $target_dir . basename($_FILES["myFile"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Verificam daca fisierul este imagine sau NU.

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["myFile"]["tmp_name"]);
    if($check !== false) {
        $errors['myFile'] = "Fisierul este o imagine - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        $errors['myFile'] = "Atenţie! Fisierul nu este o imagine.";
        $uploadOk = 0;
    }
}

// Verificam daca fisierul exista deja

if (file_exists($target_file)) {
    $errors['myFile'] = "Atenţie! Fisierul exista deja.";
    $uploadOk = 0;
}

// Verificam marimea fisierului

if ($_FILES["myFile"]["size"] > 2000000) {
    $errors['myFile'] = "Atenţie! Fisierul este prea mare";
    $uploadOk = 0;
}

// Verificam formatele acceptate 

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    $errors['myFile'] =  "Atenţie! Doar formatele JPG, JPEG, PNG şi GIF sunt acceptate.";
    $uploadOk = 0;
}

// Verificam daca fisierul a fost uploadat

if ($uploadOk == 0) {
    $errors['myFile'] = "Atenţie! Fisierul nu a fost uploadat.";

// Daca totul e O.K. vom uploada fisierul.

} else {
    if (move_uploaded_file($_FILES["myFile"]["tmp_name"], $target_file)) {
        $errors['myFile'] = "Bravo! Fisierul ". basename( $_FILES["myFile"]["name"]). " a fost uploadat cu succes.";
    } else {
        $errors['myFile'] = "Atenţie! A intervenit o eroare in timp ce fisierul a fost uploadat.";
    }
}

$e_count=count($errors);

    if ($e_count==0) {
        // DB write
    }


}
?>
<?php
/**
 * View
 */
?>
<!DOCTYPE html>
<html lang="en-US">

  <head>
    <title>Eu Sunt OM | A fi OM e arta vieţii</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link rel="stylesheet" href="css/bootstrap.min.css">   
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="css/w3.css">
    <link rel="stylesheet" type="text/css" href="css/normalize.css">  
    <link rel="stylesheet" type="text/css" href="css/style.css"> 
    
    <link rel="icon" href="images/phoenix_favicon.png" type="image/png" sizes="32x32">

  </head>
  <body>
    
    <header>
      <div class="container">
        <a href="index_main.php" class="link-transformare"><img  src="images/sigla_esom.jpg" alt="banner" class="banner1"></a>
      </div>
    </header>


    <!--Navigation menu-->
    <div class="container">
      <nav class="navbar navbar-default" role="navigation">
        
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>      
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav" >
              <li class="active"><a href="index_main.php" class="home">Acasă <span class="sr-only">(current)</span></a></li>
              <!--<li><a href="#">Articole</a></li>-->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cine sunt <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="povestea-mea.php" class="main-links">Povestea mea</a></li>
                  <li><a href="visurile-mele.php" class="main-links">Visurile mele</a></li>
                  <!--<li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#">One more separated link</a></li>-->
                </ul>
              </li>

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Articole <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="articole-viata.php" class="main-links">Viaţă</a></li>
                  <li><a href="articole-dragoste.php" class="main-links">Dragoste</a></li>
                  <li><a href="articole-diverse.php" class="main-links">Diverse</a></li>
                </ul>
              </li>

              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Membri <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="comunity.php" class="main-links">Comunitate</a></li>
                  <li><a href="register.php" class="main-links">Înscriere</a></li>
                  <li><a href="opinion.php" class="main-links">Impresii</a></li>
                </ul>
              </li>
              
              <li><a href="imagini.php" class="Other-links">Imagini</a></li>
              <li><a href="music.php" class="Other-links">Muzică</a></li>
              <li><a href="contact.php" class="Other-links">Contact</a></li>

            </ul>
          </div>
        
      </nav>
    </div> <!-- End of navigation menu-->

   

    <!--START OF Register-->

    <div class="container">
      <div class="row transformare">      
        
        <div class="col-xs-6 col-sm-4 slideshow-left-box">
          <img src="images/happiness1.jpg" class="imagini-articole" alt="picture">
        </div>

        <div class="col-xs-6 col-sm-8 slideshow-left-box">
          <p class="transform-quotation">Înscriere în comunitate</p>
          <p class="transform-quotation-register">Aici puteţi deveni membru al comunităţii noastre.</p>
        </div>

      </div>
    </div>




    <!--Formular de înregistrare-->
    
    <div class="container">
      <div class="row transformare">
        <div class="col-xs-8 col-sm-8">
          
          <p class="article-comments">
            <span class="error">* Câmpuri obligatorii</span>
              

              <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
                
                <?php
                //if (!$isValid) {
                //  print_r($errors);
                //} ?>

                <div class="form-group">
                  
                  <label for="name">Numele dumneavoastră: *</label>
                  <input type="text" class="form-control" id="name" name="name" value="<?php echo isset($_POST['name'])?$_POST['name']:'';?>">                 
                  
                  <span class="input-errors"> <?php if (isset($errors['name'])) echo $errors['name'] ?></span>
                </div>


                <div class="form-group">
                  
                  <label for="forename">Prenumele dumneavoastră: *</label>
                  <input type="text" class="form-control" id="forename" name="forename" value="<?php echo isset($_POST['forename'])?$_POST['forename']:'';?>"> 

                  <span class="input-errors"> <?php if (isset($errors['forename'])) echo $errors['forename'] ?></span>                
                
                </div>

                <div class="form-group">
                  
                  <label for="username">Nume de utilizator: *</label>
                  <input type="text" class="form-control" id="username" name="username" value="<?php echo isset($_POST['username'])?$_POST['username']:'';?>"> 

                  <span class="input-errors"> <?php if (isset($errors['username'])) echo $errors['username'] ?></span>               
                
                </div>

                <div class="form-group">
                  
                  <label for="email">E-mail - ul dumneavoastră: *</label>
                  <input type="text" class="form-control" id="email" name="email" value="<?php echo isset($_POST['email'])?$_POST['email']:'';?>">

                  <span class="input-errors"> <?php if (isset($errors['email'])) echo $errors['email'] ?></span>                
                
                </div>

                <div class="form-group">
                  
                  <label for="password">Parola dumneavoastră: *</label>
                  <input type="password" class="form-control" id="password" name="password" value="<?php echo isset($_POST['password'])?$_POST['password']:'';?>">                 
                
                  <span class="input-errors"> <?php if (isset($errors['password'])) echo $errors['password'] ?></span>

                </div>

                <div class="form-group">
                  
                  <label for="confirm_password">Confirmaţi parola dumneavoastră: *</label>
                  <input type="password" class="form-control" id="confirm_password" name="confirm_password" 
                         value="<?php echo isset($_POST['confirm_password'])?$_POST['confirm_password']:'';?>">

                  <span class="input-errors"> <?php if (isset($errors['confirm_password'])) echo $errors['confirm_password'] ?></span>
                
                </div>
                  
                <div class="form-group">
                  
                  Alegeţi sexul dumneavoastră: *
                  <input type="radio"  id="genderm" name="gender" value="m" <?php if (isset($_POST['gender']) && 'm' == $_POST['gender']) { ?> checked <?php } ?>> 

                  <label for="genderm">BĂRBAT</label>

                  <input type="radio"  id="genderf" name="gender" value="f" <?php if (isset($_POST['gender']) && 'f' == $_POST['gender']) { ?> checked <?php } ?>>

                  <label for="genderf">FEMEIE</label>


                  <br> <span class="input-errors"> <?php if (isset($errors['gender'])) echo $errors['gender'] ?></span>
                              
                </div>


                <div class="form-group">
                  
                  <label for="date_of_birth">Data naşterii: * (Format YYYY-MM-DD)</label>
                  <input type="text" class="form-control" id="date_of_birth" name="date_of_birth" 
                         value="<?php echo isset($_POST['date_of_birth'])?$_POST['date_of_birth']:'';?>">

                  <span class="input-errors"> <?php if (isset($errors['date_of_birth'])) echo $errors['date_of_birth'] ?></span>                
                
                </div>


                <div class="form-group-1">
                  
                  <label for="zodia">Alegeţi zodia dumneavoastră: * </label>

                  <select name="zodia">
                     <option value="0">Alegeţi</option>
                    <?php
                      foreach ($horoscope as $key => $value) {

                        ?><option value="<?php echo $key;?>" <?php if (isset($_POST['zodia']) && $key == $_POST['zodia']) { ?> selected <?php } ?>><?php echo htmlentities($value);?></option>
                        <?php
                        // select * from `users` where username='\' or 1=1 limit 1\;drop table users\;#' and password='jackie';
                      }
                    ?>
                  </select> 
                  
                  <br>

                  <span class="input-errors"> <?php if (isset($errors['zodia'])) echo $errors['zodia'] ?></span>

                </div>


                <div class="form-group-1">
                  
                  <label for="retea">Alegeţi reţeaua GSM: * </label>

                  <select id="retea" name="retea[]" multiple>

                    <?php
                      foreach ($retea as $key => $value) {

                        ?><option value="<?php echo $key;?>"  <?php if (isset($_POST['retea']) && in_array($key,$_POST['retea'])) { ?> selected <?php } ?>><?php echo htmlentities($value);?></option>
                        <?php
                        
                      }
                    ?>
                  </select>  

                </div>


                <div class="form-group-1">
                  
                  <label for="notes">Scrieţi o notă:</label>
                  <textarea name="notes" cols="30" rows="3"><?php echo isset($_POST['notes'])?$_POST['notes']:'';?></textarea>

                  <br><span class="input-errors"> <?php if (isset($errors['notes'])) echo $errors['notes'] ?></span>                
                
                </div>

                <div class="form-group">

                  <label for="myFile">Selectaţi o poză:</label>
                  <input type="file" id="myFile" name="myFile" value="myFile">

                  <span class="input-errors"> <?php if (isset($errors['myFile'])) echo @$errors['myFile'] ?></span>

                </div>

                <div class="form-group">

                  <label for="terms">Selectaţi pentru a accepta termenii şi condiţiile:</label>
                  <input type="checkbox" id="terms" name="terms" value="1" <?php if (isset($_POST['terms']) && 1 == $_POST['terms']) { ?> checked <?php } ?>>

                  <br><span class="input-errors"> <?php echo @$errors['terms'];?></span>     

                </div>
                
                <div class="form-group">
                  
                  <input type="submit" name="submit" value="Trimite datele" class="btn btn-success inscriere">
                
                </div>

                <div class="g-recaptcha" data-sitekey="6LelhykUAAAAAPBUWDmJTjD1iyUgnq6vsNVv-Bm1"></div>              

              </form>


          </p>
        </div>


      </div>
    </div>



    <!-- SECŢIUNEA "My signature"-->

    <div class="container">
      <div class="row signature">
        <div class="col-xs-12 signature1">
          <a href="index_main.php" class="link-transformare"><img src="images/elegant11.jpg" alt="comunitate" class="imagini-povestiri"></a>
        </div>
      </div>
    </div>
  
    

    <!-- SECŢIUNEA "Copyright" -->
    
    <div class="container">
      <div class="row signature">
        <div class="col-xs-12">
          <p class="copyright">Acest sait este proprietate privată DORGO IONEL - BOGDAN.</p>
          <p class="copyright">Orice articol sau parte dintr-un articol poate fi folosită, cu indicarea sursei iniţiale, adică 
          <a href="index_main.php" class="link-transformare">www.esom.ro</a></p>
        </div>
      </div>
    </div>


    <script src='https://www.google.com/recaptcha/api.js'></script>

  </body>
</html>